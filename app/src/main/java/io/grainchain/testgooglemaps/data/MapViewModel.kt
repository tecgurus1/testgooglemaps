package io.grainchain.testgooglemaps.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.google.android.gms.maps.model.LatLng
import io.grainchain.testgooglemaps.data.daos.TravelRepository
import io.grainchain.testgooglemaps.data.model.Travel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class MapViewModel(application: Application): AndroidViewModel(application) {
    val travelRepository: TravelRepository
    var travel: Travel? = null
    var latLngList: ArrayList<LatLng>? = null

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    init {
        val travelDAO = TravelsDB.getDatabase(application).travelDAO()
        travelRepository = TravelRepository(travelDAO)
    }

    override fun onCleared() {
        super.onCleared()
        parentJob.cancel()
    }

    fun insertTravel(travel: Travel) = scope.launch(Dispatchers.IO) {
        val idTravel = travelRepository.insetTravel(travel)
        travel.wayList.forEach { way -> way.idTravel = idTravel.toInt() }
        travelRepository.insertWay(travel.wayList)
    }
}