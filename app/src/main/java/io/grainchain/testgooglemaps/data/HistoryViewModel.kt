package io.grainchain.testgooglemaps.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import io.grainchain.testgooglemaps.data.daos.TravelRepository
import io.grainchain.testgooglemaps.data.model.Travel
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class HistoryViewModel(application: Application) : AndroidViewModel(application) {
    private val travelRepository: TravelRepository
    val allTravel: LiveData<List<Travel>>

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    init {
        val travelDAO = TravelsDB.getDatabase(application).travelDAO()
        travelRepository= TravelRepository(travelDAO)
        allTravel = travelRepository.allTravel
    }

    fun deleteTravel(travel: Travel) = scope.launch(Dispatchers.IO) {
        travelRepository.deleteWay(travel.id)
        travelRepository.deleteTravel(travel)
    }
}