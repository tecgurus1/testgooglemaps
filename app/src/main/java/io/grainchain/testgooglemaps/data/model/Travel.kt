package io.grainchain.testgooglemaps.data.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey

@Entity
data class Travel(@PrimaryKey(autoGenerate = true) @ColumnInfo(name = "id", index = true) var id: Int,
                  @ColumnInfo(name = "name") var name: String,
                  @ColumnInfo(name = "status") var isActive: Boolean,
                  @ColumnInfo(name = "distance") var distance: Double,
                  @ColumnInfo(name = "time") var time: String,
                  @Ignore var wayList: ArrayList<Way>) {
    constructor() : this(0, "", false, 0.0, "",ArrayList())
}