package io.grainchain.testgooglemaps.data.daos

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Delete
import androidx.room.Insert
import androidx.room.Query
import io.grainchain.testgooglemaps.data.model.Travel
import io.grainchain.testgooglemaps.data.model.Way

@Dao
interface TravelDAO {
    @Insert
    fun inseertTravel(travel: Travel): Long

    @Insert
    fun insertWay(wayList: ArrayList<Way>)

    @Query("SELECT * FROM Travel")
    fun getAllTravel() : LiveData<List<Travel>>

    @Delete
    fun deleteTravel(travel: Travel): Int

    @Query("DELETE FROM Way WHERE idTravel= :idTravel")
    fun deleteWay(idTravel: Int)

    @Query("SELECT * FROM Travel WHERE id = :idTravel")
    fun getTravel(idTravel: Int): LiveData<Travel>

    @Query("SELECT * FROM Way WHERE idTravel= :idTravel")
    fun getWayList(idTravel: Int): LiveData<List<Way>>
}