package io.grainchain.testgooglemaps.data

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import io.grainchain.testgooglemaps.data.daos.TravelDAO
import io.grainchain.testgooglemaps.data.model.Travel
import io.grainchain.testgooglemaps.data.model.Way

@Database(entities = [Travel::class, Way::class], version = 1)
abstract class TravelsDB: RoomDatabase() {
    abstract fun travelDAO(): TravelDAO

    companion object {
        @Volatile
        private var INSTANCE: TravelsDB? = null

        fun getDatabase(context: Context): TravelsDB {
            return INSTANCE ?: synchronized(this) {
                val instance = Room.databaseBuilder(context.applicationContext, TravelsDB::class.java, "TestMapsGrainChains").build()
                INSTANCE = instance
                return instance
            }
        }
    }
}