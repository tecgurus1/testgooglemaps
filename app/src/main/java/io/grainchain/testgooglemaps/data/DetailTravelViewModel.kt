package io.grainchain.testgooglemaps.data

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import io.grainchain.testgooglemaps.data.daos.TravelRepository
import io.grainchain.testgooglemaps.data.model.Travel
import io.grainchain.testgooglemaps.data.model.Way
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.Job
import kotlinx.coroutines.launch
import kotlin.coroutines.CoroutineContext

class DetailTravelViewModel(application: Application): AndroidViewModel(application) {
    private val travelRepository: TravelRepository

    private var parentJob = Job()
    private val coroutineContext: CoroutineContext
        get() = parentJob + Dispatchers.Main
    private val scope = CoroutineScope(coroutineContext)

    init {
        val travelDAO = TravelsDB.getDatabase(application).travelDAO()
        travelRepository = TravelRepository(travelDAO)
    }

    fun getTravel(idTravel: Int): LiveData<Travel> = travelRepository.getTravel(idTravel)

    fun getWayList(idTravel: Int): LiveData<List<Way>> = travelRepository.getWayList(idTravel)

    fun deleteTravel(travel: Travel) = scope.launch(Dispatchers.IO) {
        travelRepository.deleteWay(travel.id)
        travelRepository.deleteTravel(travel)
    }
}