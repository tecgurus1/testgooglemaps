package io.grainchain.testgooglemaps

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.content.IntentSender
import android.content.res.ColorStateList
import android.graphics.Color
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.text.InputType
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import androidx.lifecycle.ViewModelProviders
import androidx.navigation.findNavController
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.MarkerOptions
import com.google.android.gms.maps.model.PolylineOptions
import io.grainchain.testgooglemaps.data.MapViewModel
import io.grainchain.testgooglemaps.data.model.Travel
import io.grainchain.testgooglemaps.data.model.Way
import io.grainchain.testgooglemaps.util.ValidateHours

class Map : Fragment(), OnMapReadyCallback {

    private lateinit var mMap: GoogleMap
    private lateinit var lastLocation: Location
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationCallback: LocationCallback
    private lateinit var locationRequest: LocationRequest
    private var locationUpdateState = false
    private val latLngList: ArrayList<LatLng> = ArrayList()
    private lateinit var btnTrip: Button
    private lateinit var mapViewModel: MapViewModel
    private var travel: Travel? = null
    private var polylineOptions: PolylineOptions? = null

    private lateinit var startTime: String

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val mapFragment = childFragmentManager.findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity as MapsActivity)
        polylineOptions = PolylineOptions().width(5f).color(Color.BLUE).geodesic(true)

        btnTrip = view.findViewById(R.id.btnTrip)
        btnTrip.setOnClickListener {
            if (!locationUpdateState) createLocationRequest()
            else {
                btnTrip.text = resources.getString(R.string.start_trip)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
                    btnTrip.backgroundTintList = ColorStateList.valueOf(resources.getColor(android.R.color.holo_green_dark))
                else btnTrip.setBackgroundColor(resources.getColor(android.R.color.holo_green_dark))
                locationUpdateState = false
                travel?.time = ValidateHours.getDiferenceHour(startTime, ValidateHours.getHour())
                travel?.isActive = locationUpdateState
                mapViewModel.travel = travel
                fusedLocationClient.removeLocationUpdates(locationCallback)
                updateNewLocation()
                latLngList.add(LatLng(lastLocation.latitude, lastLocation.longitude))
                addNameToTravel()
            }
        }

        view.findViewById<Button>(R.id.btnHistory).setOnClickListener {
            it.findNavController().navigate(R.id.action_map_to_historyTravel)
        }
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        mapViewModel = ViewModelProviders.of(this).get(MapViewModel::class.java)
        mapViewModel.travel?.let { this.travel = it  }
    }

    override fun onMapReady(p0: GoogleMap?) {
        p0?.let {
            mMap = it
            mMap.uiSettings.isZoomControlsEnabled = true
            onCharger()
        }
    }

    private fun updateNewLocation() {
        fusedLocationClient.lastLocation.addOnSuccessListener { location ->
            if (location != null) {
                lastLocation = location
                val currentLatLng = LatLng(location.latitude, location.longitude)
                mMap.addMarker(MarkerOptions().position(currentLatLng))
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(currentLatLng, 18f))
            }
        }
    }

    private fun createLocationRequest() {
        locationRequest = LocationRequest()
        locationRequest.interval = 10000
        locationRequest.fastestInterval = 2000
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY

        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)

        val client = LocationServices.getSettingsClient(activity as MapsActivity)
        val task = client.checkLocationSettings(builder.build())

        task.addOnSuccessListener {
            locationUpdateState = true
            starDrawCurrentlyWay()
        }
        task.addOnFailureListener { e ->
            if (e is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    e.startResolutionForResult(activity as MapsActivity, 0)
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                }
            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun starDrawCurrentlyWay() {
        startTime = ValidateHours.getHour()
        travel = Travel()
        travel?.isActive = locationUpdateState
        mapViewModel.travel = travel
        latLngList.add(LatLng(lastLocation.latitude, lastLocation.longitude))
        btnTrip.text = "Detener viaje"
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            btnTrip.backgroundTintList = ColorStateList.valueOf(resources.getColor(android.R.color.holo_red_dark))
        else btnTrip.setBackgroundColor(resources.getColor(android.R.color.holo_red_dark))
        fusedLocationClient.requestLocationUpdates(locationRequest, locationCallback, null)
    }

    private fun addNameToTravel() {
        val locationFirst = Location("start")
        locationFirst.latitude = latLngList[0].latitude
        locationFirst.longitude = latLngList[0].longitude
        travel?.distance = (locationFirst.distanceTo(lastLocation).toDouble())
        val edtName = EditText(activity as MapsActivity)
        edtName.setText("")
        edtName.inputType = InputType.TYPE_TEXT_VARIATION_PERSON_NAME
        AlertDialog.Builder(activity as MapsActivity).setTitle("Test of Maps")
            .setMessage("Ingresa el nombre del viaje")
            .setView(edtName).setPositiveButton("Guardar") { _,_ ->
                travel?.let {
                    if (edtName.text.isNotEmpty()) {
                        it.name = edtName.text.toString()
                        latLngList.forEach { latLng ->
                            it.wayList.add(Way(0, 0,latLng.latitude, latLng.longitude))
                        }

                        mapViewModel.insertTravel(it)
                        mMap.clear()
                        latLngList.clear()
                        travel = null
                        polylineOptions = null
                        locationUpdateState = false
                        polylineOptions = PolylineOptions().width(5f).color(Color.BLUE).geodesic(true)
                        startTime = ""
                        onCharger()
                    } else addNameToTravel()
                }
                travel = null
                mapViewModel.travel = null
                mapViewModel.latLngList = null
                fusedLocationClient = LocationServices.getFusedLocationProviderClient(activity as MapsActivity)
            }.create().show()
    }

    private fun onCharger() {
        locationCallback = object : LocationCallback() {
            override fun onLocationResult(p0: LocationResult?) {
                super.onLocationResult(p0)
                if (locationUpdateState) {
                    p0?.let {
                        if (locationUpdateState) {
                            latLngList.add(LatLng(it.lastLocation.latitude, it.lastLocation.longitude))
                            polylineOptions?.add(LatLng(it.lastLocation.latitude, it.lastLocation.longitude))
                            mMap.addPolyline(polylineOptions)
                            mapViewModel.latLngList = latLngList
                            latLngList.forEach { latLng ->
                                mapViewModel.travel?.wayList?.add(Way(0, 0, latLng.latitude, latLng.longitude))
                            }
                            mapViewModel.travel = travel
                        }
                    }
                }
            }
        }
        mapViewModel.travel?.let {
            if (it.isActive) {
                val startMarketLatLng = LatLng(it.wayList[0].latitud, it.wayList[0].longitud)
                lastLocation = Location("location")
                lastLocation.latitude = startMarketLatLng.latitude
                lastLocation.longitude = startMarketLatLng.longitude
                mMap.addMarker(MarkerOptions().position(startMarketLatLng))
                mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(startMarketLatLng, 18f))
            } else updateNewLocation()
        }?: updateNewLocation()
        mapViewModel.latLngList?.let {
            latLngList.addAll(it)
            createLocationRequest()
        }
    }
}
